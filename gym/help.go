package gym

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"niverobot/model"
)

type Help struct{}

func NewHelloWorld() *Help {
	return &Help{}
}

func (s *Help) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Hello %s", update.Message.From.UserName))
	chatBot.Api.Send(msg)
}

func (s *Help) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	if update.Message == nil || update.Message.Text == "" {
		return false, false
	}
	return update.Message.Text == ".gym -h", false
}
