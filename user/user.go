package user

type User struct {
	ID           int64   `gorm:"primary_key;"`
	Username     *string `gorm:"default=null;"`
	FirstName    *string `gorm:"default=null;"`
	LastName     *string `gorm:"default=null;"`
	LanguageCode *string `gorm:"default=null;"`
	IsBot        bool
	GymGoal      int64 `gorm:"default:0"`
}
