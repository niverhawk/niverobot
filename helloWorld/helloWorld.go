package helloWorld

import (
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"niverobot/model"
)

type HelloWorld struct {
}

func NewHelloWorld() *HelloWorld {
	return &HelloWorld{}
}

func (s *HelloWorld) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Hello %s", update.Message.From.UserName))
	chatBot.Api.Send(msg)
}

func (s *HelloWorld) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	if update.Message == nil || update.Message.Text == "" {
		return false, false
	}
	return update.Message.Text == ".bots", true
}
