package model

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Action interface {
	Execute(update tgbotapi.Update, chatBot *ChatBot)
	TriggerAndStopProcessing(update tgbotapi.Update) (shouldTrigger bool, stopProcessing bool)
}
