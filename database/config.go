package database

import (
	"fmt"
	"os"
)

// Config Back testing Config holds configuration details for the backtester.
type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	Schema   string
}

func InitConfig() (config Config, err error) {
	config.Host, err = ReadStringEnvVar("DB_HOST")
	config.Port, err = ReadStringEnvVar("DB_PORT")
	config.Username, err = ReadStringEnvVar("DB_USER")
	config.Password, err = ReadStringEnvVar("DB_PASSWORD")
	config.Schema, err = ReadStringEnvVar("DB_SCHEMA")

	if err != nil {
		err = fmt.Errorf("err reading env var: %w", err)
		return
	}

	return
}

func ReadStringEnvVar(envVarName string) (string, error) {
	symbol := os.Getenv(envVarName)
	if symbol == "" {
		return "", fmt.Errorf("%s environment variable is not set", envVarName)
	}
	return symbol, nil
}
