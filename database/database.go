package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"niverobot/gym"
	"niverobot/user"
)

func NewDatabase() (*gorm.DB, error) {
	config, err := InitConfig()
	if err != nil {
		return nil, err
	}
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		config.Host,
		config.Username,
		config.Password,
		config.Schema,
		config.Port,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger:                                   logger.Default.LogMode(logger.Error),
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		return nil, err
	}

	// Make sure nested objects are updated on save.
	db = db.Session(&gorm.Session{FullSaveAssociations: true})
	if err := db.AutoMigrate(&user.User{}, &gym.Stats{}); err != nil {
		return nil, err
	}

	return db, nil
}
