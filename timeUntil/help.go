package timeUntil

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"niverobot/model"
	"strings"
)

type Help struct{}

func NewHelp() *Help {
	return &Help{}
}

func (t *Help) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	var sb strings.Builder
	sb.WriteString("Calculate the time between now and your input. ")
	sb.WriteString("\n")
	sb.WriteString(formattedOutputMessage())
	sb.WriteString("\n")
	sb.WriteString("To display supported formats use <b>.tu -f</b>")
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, sb.String())
	msg.ParseMode = "html"
	chatBot.Api.Send(msg)
}

func (t *Help) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	// Check for length here so we don't interfere with hourly format
	return update.Message != nil && update.Message.Text != "" &&
		strings.Contains(update.Message.Text, ".tu -h") &&
		len(strings.Split(update.Message.Text, " ")) == 2, true
}

func formattedOutputMessage() string {
	return "To set largest unit add <b>-M -d -h -m -s</b> after the <b>.tu</b> command"
}
