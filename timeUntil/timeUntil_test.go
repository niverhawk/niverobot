package timeUntil

import (
	"strings"
	"testing"
	"time"
)

func Test_calcTimeUntil(t *testing.T) {
	// StartTime= 2023-05-01 09:00:00
	testTime := time.Date(2023, 05, 02, 9, 0, 0, 0, time.UTC)
	type args struct {
		start time.Time
		end   string
	}
	tests := []struct {
		name string
		args args
		want time.Duration
	}{
		{name: "formattedDate", args: args{start: testTime, end: "02-05-2023 10:00:00"}, want: 1 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "10:08:13"}, want: 1*time.Hour + 8*time.Minute + 13*time.Second},
		{name: "formattedDate", args: args{start: testTime, end: "3-05"}, want: 15 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "3-5"}, want: 15 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "3-05-2023"}, want: 15 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "12-12-2022"}, want: -3393 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "3-5-2023"}, want: 15 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "2023-05-03"}, want: 15 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "2023-05-03 01:00:00"}, want: 16 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "12-12-12"}, want: -91041 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "01-01-01"}, want: -195753 * time.Hour},
		{name: "formattedDate", args: args{start: testTime, end: "15:04"}, want: 6*time.Hour + 4*time.Minute},
		{name: "formattedDate", args: args{start: testTime, end: "02-05 19:00"}, want: 10 * time.Hour},
		{name: "testTimeZone", args: args{start: time.Date(2023, 5, 24, 10, 0, 0, 0, time.UTC), end: "27-05-2023 09:00"}, want: 71 * time.Hour},
		{name: "testSwitchDay", args: args{start: testTime.Add(14 * time.Hour), end: "2023-05-03 01:00:00"}, want: 2 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "02-05-2023 08:00:00"}, want: -1 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "1-5"}, want: -33 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "1-05"}, want: -33 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "01-5"}, want: -33 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "01-05"}, want: -33 * time.Hour},
		// Check for past
		{name: "dateInPast", args: args{start: testTime, end: "01-05-2023 10:00:00"}, want: -23 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "7:51:47"}, want: -1*time.Hour + -8*time.Minute + -13*time.Second},
		{name: "dateInPast", args: args{start: testTime, end: "02-01"}, want: -2889 * time.Hour},
		{name: "dateInPast", args: args{start: testTime, end: "01-01-2023 10:00:00"}, want: -2903 * time.Hour},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _, _ := calcDurationDiff(tt.args.start, tt.args.end); got != tt.want {
				t.Errorf("calcDurationDiff() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_formatDuration(t *testing.T) {
	type args struct {
		duration       time.Duration
		start          string
		includeSeconds bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "ShowAll", args: args{duration: 100_265_630 * time.Second, start: "", includeSeconds: true}, want: "3 years 2 months 5 days 11 hours 33 minutes 50 seconds"},
		{name: "showYears", args: args{duration: 100_265_630 * time.Second, start: "-y", includeSeconds: true}, want: "3 years 2 months 5 days 11 hours 33 minutes 50 seconds"},
		{name: "showMonths", args: args{duration: 34_190_000 * time.Second, start: "-M", includeSeconds: true}, want: "13 months 5 days 17 hours 13 minutes 20 seconds"},
		{name: "showDays", args: args{duration: 2_768_461 * time.Second, start: "-d", includeSeconds: true}, want: "32 days 1 hours 1 minutes 1 seconds"},
		{name: "showHours", args: args{duration: 90_061 * time.Second, start: "-h", includeSeconds: true}, want: "25 hours 1 minutes 1 seconds"},
		{name: "showMinutes", args: args{duration: 3_661 * time.Second, start: "-m", includeSeconds: true}, want: "61 minutes 1 seconds"},
		{name: "ShowSeconds", args: args{duration: 61 * time.Second, start: "-s", includeSeconds: true}, want: "61 seconds"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := formatDuration(tt.args.duration, tt.args.start, tt.args.includeSeconds); !strings.EqualFold(got, tt.want) {
				t.Errorf("formatDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}
